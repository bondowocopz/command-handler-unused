"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Command = exports.Client = void 0;
var CommandHandler_1 = require("./CommandHandler");
Object.defineProperty(exports, "Client", { enumerable: true, get: function () { return CommandHandler_1.CommandHandler; } });
var BaseCommand_1 = require("./BaseCommand");
Object.defineProperty(exports, "Command", { enumerable: true, get: function () { return BaseCommand_1.BaseCommand; } });
//# sourceMappingURL=index.js.map