import { ClientOptions, Message, User } from 'discord.js';
import { BaseClient } from './BaseClient';
import { BaseCommand } from './BaseCommand';
import { ValidatorFunction } from './types/interfaces';
export declare class CommandHandler extends BaseClient {
    private allCommandsToLoad;
    private validators;
    usingCommands: Set<string>;
    constructor(options?: ClientOptions);
    private readCommandsInDirectory;
    loadCommandsInDirectory(dir: string, includesSubDirectories?: boolean): void;
    loadDefaultCommands(exclude?: Array<string>, include?: Array<string>): void;
    load(file: string): void;
    findCommand(cmd: string): BaseCommand | undefined;
    setValidator(group: string, f: ValidatorFunction): void;
    isOwner(user: User): Promise<boolean>;
    process(msg: Message): Promise<boolean>;
    private parseArg;
    private bindEvents;
}
