import { Message } from 'discord.js';
import { BaseCommand } from '../BaseCommand';

export default class Command extends BaseCommand {
  init(): void {
    this.register({
      name: 'ping',
      group: 'utility',
      description: "Check bot's latency.",
    });
  }

  async run(msg: Message): Promise<boolean> {
    const pingMsg = await msg.sendReply('Pinging...');
    pingMsg.edit(
      `🏓 Pong! Latency: ${
        (pingMsg.editedTimestamp || pingMsg.createdTimestamp) -
        (msg.editedTimestamp || msg.createdTimestamp)
      }ms. API Latency: ${Math.round(this.client.ws.ping)}ms`,
    );
    return true;
  }
}
