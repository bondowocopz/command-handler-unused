"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const CommandCooldownSchema = new mongoose_1.Schema({
    userid: String,
    command: String,
    cooldown: Number,
});
const CommandCooldown = mongoose_1.model('command_cooldowns', CommandCooldownSchema);
exports.default = CommandCooldown;
//# sourceMappingURL=CommandCooldown.js.map