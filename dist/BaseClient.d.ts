import { Client as DiscordClient, ClientOptions, Collection, Guild, GuildMember, Message, Role, TextChannel } from 'discord.js';
import { BaseCommand } from './BaseCommand';
import { CommandClientModel } from './types/interfaces';
export declare class BaseClient extends DiscordClient {
    commands: Collection<string, BaseCommand>;
    models: CommandClientModel;
    constructor(options?: ClientOptions);
    loadModels(dir: string): Promise<void>;
    loadDefaultModels(): Promise<void>;
    getDuration(time: number, time2?: number): string;
    parseGuildChannel(msg: Message, val: string): Promise<TextChannel | undefined>;
    parseRole(msg: Message, val: string): Promise<Role | undefined>;
    parseMember(msg: Message, val: string): Promise<GuildMember | undefined>;
    findGuild(id: string | undefined): Promise<Guild | undefined>;
}
