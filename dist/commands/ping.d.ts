import { Message } from 'discord.js';
import { BaseCommand } from '../BaseCommand';
export default class Command extends BaseCommand {
    init(): void;
    run(msg: Message): Promise<boolean>;
}
