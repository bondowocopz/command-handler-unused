"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseCommand = void 0;
const chalk_1 = __importDefault(require("chalk"));
class BaseCommand {
    constructor(client) {
        this.attributes = {
            name: '',
            description: '',
        };
        this.client = client;
    }
    register(attributes) {
        var _a, _b, _c, _d;
        if (!attributes.name.length) {
            throw new Error('Command name must not be empty.');
        }
        if (!attributes.group) {
            attributes.group = ((_a = attributes.groups) === null || _a === void 0 ? void 0 : _a[0]) || 'general';
        }
        if (!((_b = attributes.groups) === null || _b === void 0 ? void 0 : _b.length)) {
            attributes.groups = [attributes.group];
        }
        if (((_c = attributes.args) === null || _c === void 0 ? void 0 : _c.length) && !attributes.args.find((arg) => arg.greedy)) {
            attributes.args[attributes.args.length - 1].greedy = true;
        }
        if (!attributes.aliases) {
            attributes.aliases = [];
        }
        if (!attributes.cooldown || attributes.cooldown < 0) {
            attributes.cooldown = 5;
        }
        if (!((_d = attributes.channeltype) === null || _d === void 0 ? void 0 : _d.length)) {
            attributes.channeltype = ['dm', 'text'];
        }
        const permissions = ['SEND_MESSAGES'];
        if (attributes.permissions) {
            permissions.push(...attributes.permissions);
        }
        attributes.permissions = permissions;
        this.attributes = attributes;
        console.info(chalk_1.default `Succesfully loaded {blue.bold ${this.attributes.name}} command.`);
    }
    unload() {
        const command = this.client.commands.get(this.attributes.name);
        if (!command) {
            throw new Error(chalk_1.default `Command {red.bold ${this.attributes.name}} could not be found. ⚠️`);
        }
        this.client.commands.delete(this.attributes.name);
        if (!this.attributes.path) {
            throw new Error(chalk_1.default `Trying to unload command {red.bold ${this.attributes.name}} but path is not defined yet. ⚠️`);
        }
        delete require.cache[this.attributes.path];
        console.info(chalk_1.default `Succesfully unloaded {blue.bold ${this.attributes.name}} command. ✔️`);
    }
    reload() {
        throw new Error(chalk_1.default `Reload function is not registered on {red.bold ${this.attributes.name}}`);
    }
}
exports.BaseCommand = BaseCommand;
//# sourceMappingURL=BaseCommand.js.map