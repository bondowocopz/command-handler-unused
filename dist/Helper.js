"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_1 = require("discord.js");
discord_js_1.Message.prototype.sendReply = async function (msg) {
    var _a;
    if (typeof msg !== 'string') {
        if ((_a = msg.description) === null || _a === void 0 ? void 0 : _a.length) {
            msg.description = `${this.author}, ${msg.description[0].toLowerCase()}${msg.description.slice(1)}`;
        }
        return this.channel.send(msg);
    }
    return this.channel.send(`${msg} ${this.author}`);
};
discord_js_1.Guild.prototype.getData = async function () {
    var _a;
    return ((await ((_a = this.client.models.CommandGuild) === null || _a === void 0 ? void 0 : _a.findOne({
        guildid: this.id,
    }).exec())) || null);
};
discord_js_1.User.prototype.getCooldown = async function (command) {
    var _a;
    return ((await ((_a = this.client.models.CommandCooldown) === null || _a === void 0 ? void 0 : _a.findOne({
        userid: this.id,
        command: command.attributes.name,
    }))) || null);
};
discord_js_1.User.prototype.setCooldown = async function (command, seconds) {
    var _a;
    await ((_a = this.client.models.CommandCooldown) === null || _a === void 0 ? void 0 : _a.findOneAndUpdate({
        userid: this.id,
        command: command.attributes.name,
    }, {
        cooldown: new Date().getTime() + seconds * 1000,
    }, {
        new: true,
        upsert: true,
    }));
};
discord_js_1.User.prototype.resetCooldown = async function (command) {
    var _a;
    await ((_a = this.client.models.CommandCooldown) === null || _a === void 0 ? void 0 : _a.findOneAndUpdate({
        userid: this.id,
        command: command.attributes.name,
    }, {
        cooldown: 0,
    }, {
        new: true,
        upsert: true,
    }));
};
discord_js_1.TextChannel.prototype.getData = async function () {
    var _a, _b;
    return ((await ((_a = this.client.models.CommandChannel) === null || _a === void 0 ? void 0 : _a.findOne({
        channelid: this.id,
    }))) ||
        (await ((_b = this.client.models.CommandChannel) === null || _b === void 0 ? void 0 : _b.create({
            channelid: this.id,
        }))) ||
        null);
};
//# sourceMappingURL=Helper.js.map