import { Message, PermissionResolvable } from 'discord.js';
import { Model } from 'mongoose';
import { Command } from '..';
import { ICommandChannel } from '../models/CommandChannel';
import { ICommandCooldown } from '../models/CommandCooldown';
import { ICommandGuild } from '../models/CommandGuild';

export type ArgumentTypes =
  | 'number'
  | 'string'
  | 'member'
  | 'boolean'
  | 'channel'
  | 'guild'
  | 'role';

export type ChannelType = 'dm' | 'text' | 'news';

export interface Argument {
  name: string;
  type: ArgumentTypes;
  optional?: boolean;
  description?: string;
  greedy?: boolean;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  processor?: (
    msg: import('discord.js').Message,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    data: any
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  ) => Promise<boolean | any>;
  mustExists?: boolean;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  default?: any;
  placeholder?: string;
}

export interface CommandAttributes {
  name: string;
  description: string;
  group?: string;
  groups?: Array<string>;
  path?: string;
  args?: Array<Argument>;
  aliases?: Array<string>;
  cooldown?: number;
  channeltype?: Array<ChannelType>;
  permissions?: Array<PermissionResolvable>;
  skipChannelCheck?: boolean;
}

export type CommandClientModel = {
  CommandGuild?: Model<ICommandGuild>;
  CommandCooldown?: Model<ICommandCooldown>;
  CommandChannel?: Model<ICommandChannel>;
};

export type ValidatorFunction = (
  msg: Message,
  command: Command
) => Promise<boolean> | boolean;
