import { Guild, Message, MessageEmbed, TextChannel, User } from 'discord.js';
import { BaseCommand } from './BaseCommand';

Message.prototype.sendReply = async function (msg: string | MessageEmbed) {
  if (typeof msg !== 'string') {
    if (msg.description?.length) {
      msg.description = `${
        this.author
      }, ${msg.description[0].toLowerCase()}${msg.description.slice(1)}`;
    }
    return this.channel.send(msg);
  }
  return this.channel.send(`${msg} ${this.author}`);
};

Guild.prototype.getData = async function () {
  return (
    (await this.client.models.CommandGuild?.findOne({
      guildid: this.id,
    }).exec()) || null
  );
};

User.prototype.getCooldown = async function (command: BaseCommand) {
  return (
    (await this.client.models.CommandCooldown?.findOne({
      userid: this.id,
      command: command.attributes.name,
    })) || null
  );
};

User.prototype.setCooldown = async function (
  command: BaseCommand,
  seconds: number,
) {
  await this.client.models.CommandCooldown?.findOneAndUpdate(
    {
      userid: this.id,
      command: command.attributes.name,
    },
    {
      cooldown: new Date().getTime() + seconds * 1000,
    },
    {
      new: true,
      upsert: true,
    },
  );
};

User.prototype.resetCooldown = async function (command: BaseCommand) {
  await this.client.models.CommandCooldown?.findOneAndUpdate(
    {
      userid: this.id,
      command: command.attributes.name,
    },
    {
      cooldown: 0,
    },
    {
      new: true,
      upsert: true,
    },
  );
};

TextChannel.prototype.getData = async function () {
  return (
    (await this.client.models.CommandChannel?.findOne({
      channelid: this.id,
    })) ||
    (await this.client.models.CommandChannel?.create({
      channelid: this.id,
    })) ||
    null
  );
};
