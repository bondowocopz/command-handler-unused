"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const CommandChannelSchema = new mongoose_1.Schema({
    channelid: String,
    enabledcmds: {
        type: [String],
        default: [],
    },
    disabledcmds: {
        type: [String],
        default: [],
    },
});
const CommandChannel = mongoose_1.model('command_channels', CommandChannelSchema);
exports.default = CommandChannel;
//# sourceMappingURL=CommandChannel.js.map