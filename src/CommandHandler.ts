import chalk from 'chalk';
import {
  ClientOptions,
  Collection,
  Message,
  MessageEmbed,
  User,
} from 'discord.js';
import fs from 'fs';
import path from 'path';
import { BaseClient } from './BaseClient';
import { BaseCommand } from './BaseCommand';
import { ICommandGuild } from './models/CommandGuild';
import { Argument, ValidatorFunction } from './types/interfaces';

export class CommandHandler extends BaseClient {
  private allCommandsToLoad: Array<string> = [];
  private validators: Collection<string, ValidatorFunction> = new Collection();
  usingCommands: Set<string> = new Set();

  constructor(options?: ClientOptions) {
    super(options);
    this.bindEvents();
  }

  private readCommandsInDirectory(dir: string, includesSubDirectories = true) {
    const currentPath = dir;
    console.info(chalk`Reading commands in {blue.bold ${currentPath}}`);

    const allFiles = fs.readdirSync(currentPath);
    const allCommandFiles = allFiles.filter((file) => file.endsWith('.js'));
    const allDirectories = allFiles.filter((file) =>
      fs.lstatSync(`${currentPath}/${file}`).isDirectory(),
    );

    for (let index = 0; index < allCommandFiles.length; index++) {
      const element = allCommandFiles[index];
      this.allCommandsToLoad.push(`${currentPath}/${element}`);
    }

    if (includesSubDirectories) {
      for (let index = 0; index < allDirectories.length; index++) {
        const element = allDirectories[index];
        this.readCommandsInDirectory(
          `${currentPath}/${element}`,
          includesSubDirectories,
        );
      }
    }
  }

  loadCommandsInDirectory(dir: string, includesSubDirectories = true): void {
    this.readCommandsInDirectory(dir, includesSubDirectories);
    this.allCommandsToLoad.forEach((file) => {
      this.load(file);
    });
  }

  loadDefaultCommands(exclude?: Array<string>, include?: Array<string>): void {
    this.readCommandsInDirectory(path.resolve(__dirname, './commands'));
    this.allCommandsToLoad
      .filter((c) => {
        if (include?.length) {
          for (let index = 0; index < include.length; index++) {
            const element = include[index];
            if (c.includes(element)) {
              return true;
            }
          }
          return false;
        }
        if (exclude?.length) {
          for (let index = 0; index < exclude.length; index++) {
            const element = exclude[index];
            if (c.includes(element)) {
              return false;
            }
          }
          return true;
        }
        return true;
      })
      .forEach((file) => {
        this.load(file);
      });
  }

  load(file: string): void {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const LoadedFile = require(file).default;
    if (LoadedFile.name !== 'Command') {
      throw new Error(chalk`File {red.bold ${file}} is not a command`);
    }

    const command: BaseCommand = new LoadedFile(this);
    command.init();
    command.attributes.path = file;
    command.attributes.name = command.attributes.name.toLowerCase();
    command.reload = () => {
      command.unload();
      this.load(file);
    };

    const duplicatedFile = this.commands.get(command.attributes.name);
    if (duplicatedFile) {
      console.error(
        chalk`File {red.bold ${duplicatedFile.attributes.path}} has duplicated name attribute with {red.bold ${command.attributes.path}}.`,
      );
      process.exit(1);
    }

    this.commands.set(command.attributes.name, command);
  }

  findCommand(cmd: string): BaseCommand | undefined {
    cmd = cmd.toLowerCase();
    const command = this.commands.find(
      (c) =>
        c.attributes.name == cmd || c.attributes.aliases?.indexOf(cmd) !== -1,
    );
    return command;
  }

  setValidator(group: string, f: ValidatorFunction): void {
    if (this.validators.has(group)) {
      throw new Error(chalk`Validator for {red.bold ${group}} already defined`);
    }

    this.validators.set(group, f);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async isOwner(user: User): Promise<boolean> {
    return false;
  }

  async process(msg: Message): Promise<boolean> {
    const prefixes: Array<string> = [ process.env.PREFIX || '' ];
    const guild = msg.guild;
    let guildConfig: ICommandGuild | null = null;

    let args = msg.content.split(/ +/g);
    if (!args.length) {
      return false;
    }

    if (guild) {
      guildConfig = await guild.getData();
      if (guildConfig && guildConfig.prefix) {
        prefixes.push(guildConfig.prefix);
      }
    } else {
      prefixes.push('');
    }

    let command: BaseCommand | null = null;
    const contentToCheck = args[0].toLowerCase();
    for (let index = 0; index < prefixes.length; index++) {
      const element = prefixes[index].toLowerCase();
      if (!contentToCheck.startsWith(element)) {
        continue;
      }
      const tempContent = contentToCheck.substr(element.length);
      const tempCommand = this.findCommand(tempContent);
      if (tempCommand) {
        command = tempCommand;
        args.shift();
        break;
      }
    }

    if (!command) {
      return false;
    }

    if (command.attributes.channeltype?.indexOf(msg.channel.type) === -1) {
      return false;
    }

    if (command.attributes.groups) {
      for (let index = 0; index < command.attributes.groups.length; index++) {
        const element = command.attributes.groups[index];
        const processor = this.validators.get(element);
        if (processor) {
          const processData = await processor(msg, command);
          if (processData !== true) {
            if (typeof processData == 'string') {
              msg.sendReply(processData);
            }
            return false;
          }
        }
      }
    }

    if (!command.attributes.skipChannelCheck) {
      const isDisabledDefault = guildConfig ?
        guildConfig.globallyDisabled.indexOf(command.attributes.name) !== -1 ?
          true :
          guildConfig.globallyDisabled.indexOf('all') !== -1 :
        false;
      if (msg.channel.type == 'text') {
        const channelData = await msg.channel.getData();
        if (channelData) {
          if (
            isDisabledDefault &&
            channelData.enabledcmds.indexOf(command.attributes.name) === -1 &&
            channelData.enabledcmds.indexOf('all') === -1
          ) {
            return false;
          }

          if (
            channelData.disabledcmds.indexOf(command.attributes.name) !== -1 ||
            channelData.disabledcmds.indexOf('all') !== -1
          ) {
            return false;
          }
        }
      }
    }

    const currentTimestamp = new Date().getTime();
    const cooldown = await msg.author.getCooldown(command);
    if (
      cooldown &&
      cooldown.cooldown > currentTimestamp &&
      !(await this.isOwner(msg.author))
    ) {
      msg.sendReply(
        `Please wait **${this.getDuration(
          cooldown.cooldown,
        )}** before using this command.`,
      );
      return false;
    }

    if (
      command.attributes.permissions &&
      msg.channel.type != 'dm' &&
      msg.guild &&
      msg.guild.me
    ) {
      for (
        let index = 0;
        index < command.attributes.permissions.length;
        index++
      ) {
        const element = command.attributes.permissions[index];
        const clientMember = msg.guild.me;
        if (!msg.channel.permissionsFor(clientMember)?.has(element)) {
          const channel = msg.channel;
          msg.author
            .createDM()
            .then((c) => {
              const embed = new MessageEmbed({
                color: '#DD0000',
              });
              embed
                .setTitle(
                  `Failed to execute ${command?.attributes.name} command.`,
                )
                .setDescription(
                  `I have no privilege to \`${element}\` on **#${channel.name}**.`,
                );
              return c.send(embed);
            })
            .catch(() => {
              if (channel.permissionsFor(clientMember)?.has('ADD_REACTIONS')) {
                msg.react('⚠️').catch(() => {
                  // Does nothing
                });
              }
            });
        }
      }
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const parsedArgs: any = {};
    if (command.attributes.args?.length) {
      const requiredData = command.attributes.args.filter(
        (a) => a.optional !== true,
      );
      args = args.filter((a) => a.length > 0);
      if (args.length < requiredData.length) {
        msg.sendReply(
          `Invalid command usage, correct usage:\n\`\`\`md\n${prefixes[0]}${
            command.attributes.name
          } ${command.attributes.args
            .map((a) => `<${a.placeholder || a.name}${a.optional ? '?' : ''}>`)
            .join(' ')}\`\`\``,
        );
        return false;
      }

      for (let index = 0; index < command.attributes.args.length; index++) {
        const element = command.attributes.args[index];
        if (element.optional && args.length < requiredData.length) {
          parsedArgs[element.name] = element.default;
          continue;
        }

        if (
          index == command.attributes.args.length - 1 &&
          element.optional &&
          !args.length
        ) {
          parsedArgs[element.name] = element.default;
          continue;
        }

        let parsedArg: string | boolean = false;
        if (element.greedy) {
          const sliceEnd = (command.attributes.args.length - 1 - index) * -1;
          const text = args.splice(
            0,
            args.slice(0, sliceEnd === 0 ? undefined : sliceEnd).length,
          );
          parsedArg = await this.parseArg(element, text.join(' '), msg);
        } else {
          let currentArg = args.shift();
          if (typeof currentArg !== 'string') {
            currentArg = '';
          }
          parsedArg = await this.parseArg(element, currentArg, msg);
        }

        if (parsedArg === undefined) {
          return false;
        }
        parsedArgs[element.name] = parsedArg;
        if (!element.optional) {
          requiredData.shift();
        }
      }
    }

    try {
      const returnedCommand = await command.run(msg, parsedArgs);
      if (returnedCommand && command.attributes.cooldown) {
        await msg.author.setCooldown(command, command.attributes.cooldown);
      }
    } catch (e) {
      msg.sendReply(
        "Looks like there's an error whilst trying to run that command.",
      );
      console.log(
        chalk`Error while running {red.bold ${command.attributes.name}} command with content {red.bold ${msg.content}}`,
        e,
      );
      return false;
    }
    this.emit('commandSuccess', msg, command);
    return true;
  }

  private async parseArg(arg: Argument, text: string, msg: Message) {
    let currentData;
    switch (arg.type) {
      case 'boolean': {
        const positive = [ '1', 'true', 't', 'yes', 'y', 'on' ];
        const negative = [ '0', 'false', 'f', 'no', 'n', 'off' ];
        if (positive.indexOf(text.toLowerCase()) !== -1) {
          return true;
        } else if (negative.indexOf(text.toLowerCase()) !== -1) {
          return false;
        }

        msg.sendReply(`Invalid boolean provided for "${arg.name}".`);
        return undefined;
      }

      case 'number': {
        if (!/^\d+$/.test(text)) {
          msg.sendReply(`Invalid number provided for "${arg.name}".`);
          return;
        }

        const integer = parseInt(text);
        if (integer != integer) {
          msg.sendReply(`Invalid number provided for "${arg.name}".`);
          return;
        }

        currentData = integer;
        break;
      }

      case 'string': {
        currentData = text;
        break;
      }

      case 'channel': {
        const channel = await this.parseGuildChannel(msg, text);
        if (!channel) {
          if (arg.mustExists !== false) {
            msg.sendReply(`Invalid channel provided for "${arg.name}"`);
            return;
          }

          currentData = text;
        } else {
          currentData = channel;
        }
        break;
      }

      case 'role': {
        const role = await this.parseRole(msg, text);
        if (!role) {
          if (arg.mustExists !== false) {
            msg.sendReply(`Invalid role provided for "${arg.name}".`);
            return;
          } else {
            currentData = text;
          }
        } else {
          currentData = role;
        }

        break;
      }

      case 'member': {
        const member = await this.parseMember(msg, text);
        if (!member) {
          if (arg.mustExists !== false) {
            msg.sendReply(`Invalid member provided for "${arg.name}".`);
            return;
          } else {
            currentData = text;
          }
        } else {
          currentData = member;
        }

        break;
      }

      case 'guild': {
        const guild = await this.findGuild(text);
        if (!guild) {
          if (arg.mustExists !== false) {
            msg.sendReply(`Invalid guild provided for "${arg.name}".`);
            return;
          } else {
            currentData = text;
          }
        } else {
          currentData = guild;
        }
        break;
      }
    }

    if (typeof arg.processor === 'function') {
      const functionReturn = await arg.processor(msg, currentData);
      return functionReturn;
    }

    return currentData;
  }

  private bindEvents() {
    this.on('message', async (msg) => {
      if (msg.author.bot) {
        return;
      }

      if (!this.usingCommands.has(msg.author.id)) {
        this.usingCommands.add(msg.author.id);
        await this.process(msg);
        this.usingCommands.delete(msg.author.id);
      }

      if (
        msg.author.id == process.env.OWNER &&
        process.env.NODE_ENV !== 'production'
      ) {
        console.log(chalk`{blue.bold ${msg.author.tag}:} ${msg.content}`);
      }
    });
  }
}
