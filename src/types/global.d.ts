declare module 'discord.js' {
  interface Message {
    client: import('../BaseClient').BaseClient;
    sendReply(
      msg: string | import('discord.js').MessageEmbed
    ): Promise<Message>;
  }

  interface Guild {
    client: import('../BaseClient').BaseClient;
    getData(): Promise<import('../models/CommandGuild').ICommandGuild | null>;
  }

  interface User {
    client: import('../BaseClient').BaseClient;
    getCooldown(
      command: import('../BaseCommand').BaseCommand
    ): Promise<import('../models/CommandCooldown').ICommandCooldown | null>;
    setCooldown(
      command: import('../BaseCommand').BaseCommand,
      seconds: number
    ): Promise<void>;
    resetCooldown(command: import('../BaseCommand').BaseCommand): Promise<void>;
  }

  interface TextChannel {
    client: import('../BaseClient').BaseClient;
    getData(): Promise<
      import('../models/CommandChannel').ICommandChannel | null
    >;
  }

  interface ClientEvents extends import('discord.js').ClientEvents {
    commandSuccess: [Message, import('../BaseCommand').BaseCommand];
  }
}
