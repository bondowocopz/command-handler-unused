"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BaseCommand_1 = require("../BaseCommand");
class Command extends BaseCommand_1.BaseCommand {
    init() {
        this.register({
            name: 'ping',
            group: 'utility',
            description: "Check bot's latency.",
        });
    }
    async run(msg) {
        const pingMsg = await msg.sendReply('Pinging...');
        pingMsg.edit(`🏓 Pong! Latency: ${(pingMsg.editedTimestamp || pingMsg.createdTimestamp) -
            (msg.editedTimestamp || msg.createdTimestamp)}ms. API Latency: ${Math.round(this.client.ws.ping)}ms`);
        return true;
    }
}
exports.default = Command;
//# sourceMappingURL=ping.js.map