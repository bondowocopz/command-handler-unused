import chalk from 'chalk';
import { Message, PermissionResolvable } from 'discord.js';
import { CommandAttributes } from './types/interfaces';
import { BaseClient } from './BaseClient';

export abstract class BaseCommand {
  client: BaseClient;
  attributes: CommandAttributes = {
    name: '',
    description: '',
  };

  constructor(client: BaseClient) {
    this.client = client;
  }

  register(attributes: CommandAttributes): void {
    if (!attributes.name.length) {
      throw new Error('Command name must not be empty.');
    }

    if (!attributes.group) {
      attributes.group = attributes.groups?.[0] || 'general';
    }

    if (!attributes.groups?.length) {
      attributes.groups = [ attributes.group ];
    }

    if (attributes.args?.length && !attributes.args.find((arg) => arg.greedy)) {
      attributes.args[attributes.args.length - 1].greedy = true;
    }

    if (!attributes.aliases) {
      attributes.aliases = [];
    }

    if (!attributes.cooldown || attributes.cooldown < 0) {
      attributes.cooldown = 5;
    }

    if (!attributes.channeltype?.length) {
      attributes.channeltype = [ 'dm', 'text' ];
    }

    const permissions: Array<PermissionResolvable> = [ 'SEND_MESSAGES' ];
    if (attributes.permissions) {
      permissions.push(...attributes.permissions);
    }
    attributes.permissions = permissions;

    this.attributes = attributes;

    console.info(
      chalk`Succesfully loaded {blue.bold ${this.attributes.name}} command.`,
    );
  }

  abstract init(): Promise<void> | void;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars,@typescript-eslint/explicit-module-boundary-types,@typescript-eslint/no-explicit-any
  abstract run(message: Message, args: any): Promise<boolean>;

  unload(): void {
    const command = this.client.commands.get(this.attributes.name);
    if (!command) {
      throw new Error(
        chalk`Command {red.bold ${this.attributes.name}} could not be found. ⚠️`,
      );
    }

    this.client.commands.delete(this.attributes.name);
    if (!this.attributes.path) {
      throw new Error(
        chalk`Trying to unload command {red.bold ${this.attributes.name}} but path is not defined yet. ⚠️`,
      );
    }

    delete require.cache[this.attributes.path];
    console.info(
      chalk`Succesfully unloaded {blue.bold ${this.attributes.name}} command. ✔️`,
    );
  }

  reload(): void {
    throw new Error(
      chalk`Reload function is not registered on {red.bold ${this.attributes.name}}`,
    );
  }
}
