import { Schema, model, Document } from 'mongoose';

export interface ICommandGuild extends Document {
  guildid: string;
  prefix: string;
  globallyDisabled: Array<string>;
  globallyEnabled: Array<string>;
}

const CommandGuildSchema = new Schema({
  guildid: String,
  prefix: String,
  globallyDisabled: {
    type: [ String ],
    default: [],
  },
  globallyEnabled: {
    type: [ String ],
    default: [],
  },
});

const CommandGuild = model<ICommandGuild>('command_guild', CommandGuildSchema);

export default CommandGuild;
