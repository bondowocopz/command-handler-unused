import { Schema, model, Document } from 'mongoose';

export interface ICommandCooldown extends Document {
  userid: string;
  command: string;
  cooldown: number;
}

const CommandCooldownSchema = new Schema({
  userid: String,
  command: String,
  cooldown: Number,
});

const CommandCooldown = model<ICommandCooldown>(
  'command_cooldowns',
  CommandCooldownSchema,
);

export default CommandCooldown;
