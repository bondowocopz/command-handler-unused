import { Message } from 'discord.js';
import { CommandAttributes } from './types/interfaces';
import { BaseClient } from './BaseClient';
export declare abstract class BaseCommand {
    client: BaseClient;
    attributes: CommandAttributes;
    constructor(client: BaseClient);
    register(attributes: CommandAttributes): void;
    abstract init(): Promise<void> | void;
    abstract run(message: Message, args: any): Promise<boolean>;
    unload(): void;
    reload(): void;
}
