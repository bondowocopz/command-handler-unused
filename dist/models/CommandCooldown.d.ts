import { Document } from 'mongoose';
export interface ICommandCooldown extends Document {
    userid: string;
    command: string;
    cooldown: number;
}
declare const CommandCooldown: import("mongoose").Model<ICommandCooldown, {}>;
export default CommandCooldown;
