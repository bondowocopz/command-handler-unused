import { Document } from 'mongoose';
export interface ICommandChannel extends Document {
    channelid: string;
    enabledcmds: Array<string>;
    disabledcmds: Array<string>;
}
declare const CommandChannel: import("mongoose").Model<ICommandChannel, {}>;
export default CommandChannel;
