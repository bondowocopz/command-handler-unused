"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseClient = void 0;
const chalk_1 = __importDefault(require("chalk"));
const discord_js_1 = require("discord.js");
const fs_1 = require("fs");
const moment_1 = __importDefault(require("moment"));
const path_1 = __importDefault(require("path"));
class BaseClient extends discord_js_1.Client {
    constructor(options) {
        super(options);
        this.commands = new discord_js_1.Collection();
        this.models = {};
    }
    async loadModels(dir) {
        const allFiles = fs_1.readdirSync(dir).filter((file) => file.endsWith('.js'));
        for (let index = 0; index < allFiles.length; index++) {
            const element = allFiles[index];
            const splitName = element.split('/');
            // eslint-disable-next-line @typescript-eslint/no-var-requires
            const model = require(`${dir}/${element}`).default;
            const modelName = splitName[splitName.length - 1].replace('.js', '');
            this.models[modelName] = model;
            console.info(chalk_1.default `Model {blue.bold ${modelName}} succesfully loaded.`);
        }
    }
    async loadDefaultModels() {
        return await this.loadModels(path_1.default.resolve(__dirname, './models'));
    }
    getDuration(time, time2 = new Date().getTime()) {
        const diff = moment_1.default(time).diff(moment_1.default(time2));
        const duration = moment_1.default.duration(diff);
        const toCheck = ['days', 'hours', 'minutes', 'seconds'];
        for (let index = 0; index < toCheck.length; index++) {
            const element = toCheck[index];
            const currentDuration = duration[element]();
            if (currentDuration > 0) {
                return `${currentDuration} ${currentDuration > 1 ? element : element.slice(0, -1)}`;
            }
        }
        return 'about now';
    }
    async parseGuildChannel(msg, val) {
        if (!msg.guild)
            return;
        const channel = msg.guild.channels.cache.get(val.replace(/[\<\>\#]+/g, ''));
        if (channel) {
            if (channel.type === 'text') {
                return channel;
            }
            else {
                return;
            }
        }
        const search = val.toLowerCase();
        const channels = msg.guild.channels.cache.filter((c) => c.name.toLowerCase().includes(search));
        if (!channels.size)
            return;
        if (channels.size === 0)
            return;
        if (channels.size === 1) {
            const firstChannel = channels.first();
            if ((firstChannel === null || firstChannel === void 0 ? void 0 : firstChannel.type) === 'text') {
                return firstChannel;
            }
            else {
                return;
            }
        }
        const exactChannel = channels.filter((c) => c.name.toLowerCase() === search);
        if (exactChannel.size === 1) {
            const firstChannel = exactChannel.first();
            if ((firstChannel === null || firstChannel === void 0 ? void 0 : firstChannel.type) === 'text') {
                return firstChannel;
            }
            else {
                return;
            }
        }
        return;
    }
    async parseRole(msg, val) {
        if (!msg.guild)
            return;
        const role = msg.guild.roles.cache.get(val.replace(/[\<\>\@\&]+/g, ''));
        if (role) {
            return role;
        }
        const search = val.toLowerCase();
        const roles = msg.guild.roles.cache.filter((c) => c.name.toLowerCase().includes(search));
        if (!roles.size)
            return;
        if (roles.size === 0)
            return;
        if (roles.size === 1) {
            const firstRole = roles.first();
            return firstRole;
        }
        const exactRole = roles.filter((c) => c.name.toLowerCase() === search);
        if (exactRole.size === 1) {
            const firstRole = exactRole.first();
            return firstRole;
        }
        return;
    }
    async parseMember(msg, val) {
        if (!msg.guild)
            return;
        const matches = val.match(/^(?:<@!?)?([0-9]+)>?$/);
        if (matches) {
            try {
                return await msg.guild.members.fetch(matches[1]);
            }
            catch (e) {
                return;
            }
        }
        const search = val.toLowerCase();
        const members = msg.guild.members.cache.filter((mem) => mem.user.username.toLowerCase() === search ||
            (mem.nickname && mem.nickname.toLowerCase() === search) ||
            `${mem.user.username.toLowerCase()}#${mem.user.discriminator}` ===
                search);
        if (!members.size)
            return;
        if (members.size === 0)
            return;
        if (members.size === 1)
            return members.first();
        const exactMembers = members.filter((mem) => mem.user.username.toLowerCase().includes(search) ||
            (mem.nickname && mem.nickname.toLowerCase().includes(search)) ||
            `${mem.user.username.toLowerCase()}#${mem.user.discriminator}`.includes(search));
        if (exactMembers.size === 1)
            return exactMembers.first();
        return;
    }
    async findGuild(id) {
        if (!id) {
            return;
        }
        const temp = this.guilds.cache.get(id);
        return temp;
    }
}
exports.BaseClient = BaseClient;
//# sourceMappingURL=BaseClient.js.map