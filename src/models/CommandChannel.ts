import { Schema, model, Document } from 'mongoose';

export interface ICommandChannel extends Document {
  channelid: string;
  enabledcmds: Array<string>;
  disabledcmds: Array<string>;
}

const CommandChannelSchema = new Schema({
  channelid: String,
  enabledcmds: {
    type: [ String ],
    default: [],
  },
  disabledcmds: {
    type: [ String ],
    default: [],
  },
});

const CommandChannel = model<ICommandChannel>(
  'command_channels',
  CommandChannelSchema,
);

export default CommandChannel;
