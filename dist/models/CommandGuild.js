"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const CommandGuildSchema = new mongoose_1.Schema({
    guildid: String,
    prefix: String,
    globallyDisabled: {
        type: [String],
        default: [],
    },
    globallyEnabled: {
        type: [String],
        default: [],
    },
});
const CommandGuild = mongoose_1.model('command_guild', CommandGuildSchema);
exports.default = CommandGuild;
//# sourceMappingURL=CommandGuild.js.map