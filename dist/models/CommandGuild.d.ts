import { Document } from 'mongoose';
export interface ICommandGuild extends Document {
    guildid: string;
    prefix: string;
    globallyDisabled: Array<string>;
    globallyEnabled: Array<string>;
}
declare const CommandGuild: import("mongoose").Model<ICommandGuild, {}>;
export default CommandGuild;
