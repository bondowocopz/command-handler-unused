import chalk from 'chalk';
import {
  Client as DiscordClient,
  ClientOptions,
  Collection,
  Guild,
  GuildMember,
  Message,
  Role,
  TextChannel,
} from 'discord.js';
import { readdirSync } from 'fs';
import moment from 'moment';
import path from 'path';
import { BaseCommand } from './BaseCommand';
import { CommandClientModel } from './types/interfaces';
export class BaseClient extends DiscordClient {
  commands: Collection<string, BaseCommand> = new Collection();
  models: CommandClientModel = {};
  constructor(options?: ClientOptions) {
    super(options);
  }

  async loadModels(dir: string): Promise<void> {
    const allFiles = readdirSync(dir).filter((file) => file.endsWith('.js'));
    for (let index = 0; index < allFiles.length; index++) {
      const element = allFiles[index];
      const splitName = element.split('/');
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const model = require(`${dir}/${element}`).default;
      const modelName = splitName[splitName.length - 1].replace(
        '.js',
        '',
      ) as keyof CommandClientModel;
      this.models[modelName] = model;
      console.info(chalk`Model {blue.bold ${modelName}} succesfully loaded.`);
    }
  }

  async loadDefaultModels(): Promise<void> {
    return await this.loadModels(path.resolve(__dirname, './models'));
  }

  getDuration(time: number, time2: number = new Date().getTime()): string {
    const diff = moment(time).diff(moment(time2));
    const duration = moment.duration(diff);
    const toCheck = [ 'days', 'hours', 'minutes', 'seconds' ];
    for (let index = 0; index < toCheck.length; index++) {
      const element = <'days' | 'hours' | 'minutes' | 'seconds'>toCheck[index];
      const currentDuration = duration[element]();
      if (currentDuration > 0) {
        return `${currentDuration} ${
          currentDuration > 1 ? element : element.slice(0, -1)
        }`;
      }
    }
    return 'about now';
  }

  async parseGuildChannel(
    msg: Message,
    val: string,
  ): Promise<TextChannel | undefined> {
    if (!msg.guild) return;
    const channel = msg.guild.channels.cache.get(val.replace(/[\<\>\#]+/g, ''));
    if (channel) {
      if (channel.type === 'text') {
        return <TextChannel>channel;
      } else {
        return;
      }
    }
    const search = val.toLowerCase();
    const channels = msg.guild.channels.cache.filter((c) =>
      c.name.toLowerCase().includes(search),
    );
    if (!channels.size) return;
    if (channels.size === 0) return;
    if (channels.size === 1) {
      const firstChannel = channels.first();
      if (firstChannel?.type === 'text') {
        return <TextChannel>firstChannel;
      } else {
        return;
      }
    }

    const exactChannel = channels.filter(
      (c) => c.name.toLowerCase() === search,
    );
    if (exactChannel.size === 1) {
      const firstChannel = exactChannel.first();
      if (firstChannel?.type === 'text') {
        return <TextChannel>firstChannel;
      } else {
        return;
      }
    }
    return;
  }

  async parseRole(msg: Message, val: string): Promise<Role | undefined> {
    if (!msg.guild) return;
    const role = msg.guild.roles.cache.get(val.replace(/[\<\>\@\&]+/g, ''));
    if (role) {
      return role;
    }
    const search = val.toLowerCase();
    const roles = msg.guild.roles.cache.filter((c) =>
      c.name.toLowerCase().includes(search),
    );
    if (!roles.size) return;
    if (roles.size === 0) return;
    if (roles.size === 1) {
      const firstRole = roles.first();
      return firstRole;
    }

    const exactRole = roles.filter((c) => c.name.toLowerCase() === search);
    if (exactRole.size === 1) {
      const firstRole = exactRole.first();
      return firstRole;
    }
    return;
  }

  async parseMember(
    msg: Message,
    val: string,
  ): Promise<GuildMember | undefined> {
    if (!msg.guild) return;
    const matches = val.match(/^(?:<@!?)?([0-9]+)>?$/);
    if (matches) {
      try {
        return await msg.guild.members.fetch(matches[1]);
      } catch (e) {
        return;
      }
    }
    const search = val.toLowerCase();
    const members = msg.guild.members.cache.filter(
      (mem) =>
        mem.user.username.toLowerCase() === search ||
        (mem.nickname && mem.nickname.toLowerCase() === search) ||
        `${mem.user.username.toLowerCase()}#${mem.user.discriminator}` ===
          search,
    );
    if (!members.size) return;
    if (members.size === 0) return;
    if (members.size === 1) return members.first();
    const exactMembers = members.filter(
      (mem) =>
        mem.user.username.toLowerCase().includes(search) ||
        (mem.nickname && mem.nickname.toLowerCase().includes(search)) ||
        `${mem.user.username.toLowerCase()}#${mem.user.discriminator}`.includes(
          search,
        ),
    );
    if (exactMembers.size === 1) return exactMembers.first();
    return;
  }

  async findGuild(id: string | undefined): Promise<Guild | undefined> {
    if (!id) {
      return;
    }
    const temp = this.guilds.cache.get(id);
    return temp;
  }
}
